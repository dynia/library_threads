#ifndef __LIBRARY_THREADS_H
#define __LIBRARY_THREADS_H

#include <stdint.h>

typedef uint64_t lthread_t;

struct thread_info;

typedef struct
{
	struct thread_info * locker;
	struct list_node * thread_queue_front;
	struct list_node * thread_queue_back;
} lthread_mutex_t;

int lthread_init();

int lthread_finish();

int lthread_create(lthread_t *, void *(*start_routine)(void*), void * arg);

int lthread_join(lthread_t , void **value_ptr);

void lthread_exit(void *value);

lthread_t lthread_self();

void lsched_yield();

int lthread_mutex_lock(lthread_mutex_t *);

int lthread_mutex_unlock(lthread_mutex_t *);

/* TODO missing:
int lthread_cond_wait(lthread_cond_t *, lthread_mutex_t *);
int lthread_cond_signal(lthread_cond_t *);
int lthread_cond_broadcast(lthread_cond_t *);
int lthread_kill(lthread_t tid);
*/
#define LTHREAD_MUTEX_INITIALIZER { 0, 0, 0}


#endif // __LIBRARY_THREADS_H
