all:
	gcc -Wall -Werror -fPIC -c library_threads.c
	gcc -shared -Wl,-soname,liblthreads.so.0.1 -o liblthreads.so.0.1 *.o
	ln -sf liblthreads.so.0.1 liblthreads.so
.PHONY: clean
clean:
	@rm -f liblthreads.so*
	@rm -f *.o
