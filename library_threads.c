#include "library_threads.h"
#include <ucontext.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <assert.h>
#include <stdint.h>

#define die(msg) do { perror((msg)); exit(EXIT_FAILURE); } while (0)
#define abort_if(cond, msg) if ((cond)) { printf("%s\n", (msg)); abort(); }

typedef uint32_t uint32;
typedef int32_t int32;

static const uint32 THREAD_STACK_SIZE = 4*1024*1024;
static const uint32 SCHED_STACK_SIZE = 4*1024;
static const uint32 TICK_MSEC = 1;

enum thread_state
{
	RUNNABLE = 0,
	WAITING,
	DONE
};

struct list_node
{
    struct thread_info * thread;
    struct list_node * next;
};
typedef struct list_node thread_list_t;

typedef struct thread_info
{
	lthread_t id;
	void *(*routine)(void *);
	void * arg;
	void * result;
	ucontext_t context;
	struct thread_info * next;
	enum thread_state state;
} thread_info_t;

typedef struct
{
	lthread_t tid_cnt;
	thread_info_t * curr_thread;
	thread_info_t * last_thread;
	thread_info_t * threads;
	ucontext_t scheduler_ctx;
	char scheduler_started;
} thread_pool_t;

//==========================================//

static void block_sigalarm()
{
        sigset_t ss;
        sigemptyset(&ss);
        sigaddset(&ss, SIGVTALRM);
        sigprocmask(SIG_BLOCK, &ss, NULL);
}

static void switch_context(int );

static void set_alarm(uint32 slice)
{
	signal(SIGVTALRM, switch_context);
	struct itimerval tick;
	tick.it_value.tv_sec = 0;
	tick.it_value.tv_usec = slice;
	if (setitimer(ITIMER_VIRTUAL, &tick, NULL) == -1)
        die("setitimer");
}

// TODO: implement dead lock checking (i.e. all non-DONE are WAITING, is it possible?) ???
static thread_info_t * find_next_runnable(thread_pool_t * pool)
{
	if (!pool->curr_thread)
	{
		return pool->threads;
	}
	thread_info_t * it = pool->curr_thread->next;

	while (it != pool->curr_thread && it->state != RUNNABLE)
	{
		it = it->next;
	}

	return it->state == RUNNABLE ? it : NULL;
}

// linear RR, yeah!
static void schedule(thread_pool_t * pool)
{
    block_sigalarm();

	if (pool->curr_thread && DONE == pool->curr_thread->state)
	{
		free(pool->curr_thread->context.uc_stack.ss_sp);
	}

	while ((pool->curr_thread = find_next_runnable(pool)) != NULL)
	{
		set_alarm(TICK_MSEC);
		swapcontext(&pool->scheduler_ctx, &pool->curr_thread->context);
	}
	pool->scheduler_started = 0;
}

static void make_context(ucontext_t * ctx, void (routine)(void *), void * arg, ucontext_t * next_ctx, uint32 stack_size);
static int add_new_thread(thread_pool_t * pool, lthread_t * id, void *(*start_routine)(void*), void * arg);

static void start_scheduler(thread_pool_t * pool)
{
	//block_sigalarm();
	assert(!pool->threads && !pool->curr_thread && !pool->last_thread);

	pool->scheduler_started = 1;
	make_context(&pool->scheduler_ctx, (void (*)(void *)) schedule, pool, NULL, SCHED_STACK_SIZE);

	lthread_t tid;
	add_new_thread(pool, &tid, NULL, NULL);
	assert(pool->threads && tid == 0);

	set_alarm(TICK_MSEC);
	if (0 != swapcontext(&pool->threads->context, &pool->scheduler_ctx))
        die("swapcontext");
}

static void thread_done(thread_info_t * thread, void * result)
{
	thread->result = result;
	thread->routine = NULL;
	thread->state = DONE;
}

static void run_routine(thread_info_t * thread)
{
	void * result = thread->routine(thread->arg);
	thread_done(thread, result);
}

static lthread_t add_new(thread_pool_t * pool, thread_info_t * info, void *(start_routine)(void *), void * arg)
{
	info->id = pool->tid_cnt++;
	info->routine = start_routine;
	info->arg = arg;
	make_context(&info->context, (void (*)(void *)) run_routine, info, &pool->scheduler_ctx, THREAD_STACK_SIZE);

	if (!pool->threads)
	{
		assert(!pool->last_thread);
		pool->threads = pool->last_thread = info->next = info;
	}
	else
	{
		assert(pool->last_thread->next == pool->threads);
		info->next = pool->last_thread->next;
		pool->last_thread->next = info;
		pool->last_thread = info;
	}

	return info->id;
}

static int add_new_thread(thread_pool_t * pool, lthread_t * id, void *(*start_routine)(void*), void * arg)
{
	//block_sigalarm();

	thread_info_t * thread = malloc(sizeof(thread_info_t));
	lthread_t const new_id  = add_new(pool, thread, start_routine, arg);
	*id = new_id;
	return 0;
}

static void make_context(ucontext_t * ctx, void (routine)(void *), void * arg, ucontext_t * next_ctx, uint32 stack_size)
{
	if (-1 == getcontext(ctx)) die("getcontext");

	ctx->uc_stack.ss_sp = malloc(stack_size);
    	ctx->uc_stack.ss_size = stack_size;
	ctx->uc_link = next_ctx;
	makecontext(ctx,  (void (*)(void) )routine, 1, arg);
}

//================ globals ==============================//

static thread_pool_t thread_pool;

static void switch_context(int sig_num)
{
        block_sigalarm();
        swapcontext(&thread_pool.curr_thread->context, &thread_pool.scheduler_ctx);
}

thread_info_t * get_curr_thread()
{
	abort_if(!thread_pool.scheduler_started, "lthreads not initialized");
	return thread_pool.curr_thread;
}

//============== interface functions ===================//

int lthread_init()
{
	abort_if(thread_pool.scheduler_started, "lthreads already initialized");
	start_scheduler(&thread_pool);
	return 0;
}

lthread_t lthread_self()
{
	return get_curr_thread()->id;
}

int lthread_create(lthread_t * thread_id, void *(*start_routine)(void*), void * arg)
{
	abort_if(!thread_pool.scheduler_started, "lthreads not initialized");
	return add_new_thread(&thread_pool, thread_id, start_routine, arg);
}

void lthread_exit(void *value)
{
	thread_done(get_curr_thread(), value);
}

int lthread_join(lthread_t thread_id, void **value_ptr)
{
	assert(thread_id != get_curr_thread()->id);

	thread_info_t * it = get_curr_thread()->next;
    // TODO: check if thread we waiting on exists in debug
	
	while (it->id != thread_id)
	{
		it = it->next;
	}

	while (it->state != DONE)
	{
		lsched_yield();
	}

	if (value_ptr)
	{
		*value_ptr = it->result;
	}

	// TODO: remove the dead guy

	return 0;
}

void lsched_yield()
{
	swapcontext(&thread_pool.curr_thread->context, &thread_pool.scheduler_ctx);
}

int lthread_mutex_lock(lthread_mutex_t * mtx)
{
	if (!mtx->locker)
	{
		mtx->locker = get_curr_thread();
	}
	else
	{
		assert(mtx->locker != get_curr_thread()); // deadlock
		thread_list_t * node = malloc(sizeof(thread_list_t));

		node->thread = get_curr_thread();
		node->next = NULL;

		if (mtx->thread_queue_front)
		{
			assert(mtx->thread_queue_back);
			mtx->thread_queue_back->next = node;
			mtx->thread_queue_back = node;
		}
		else
		{
			assert(!mtx->thread_queue_back);
			mtx->thread_queue_front = mtx->thread_queue_back = node;
		}

		node->thread->state = WAITING;
		lsched_yield();
	}
	return 0;
}

//TODO: implement POSIX wake up semantic
int lthread_mutex_unlock(lthread_mutex_t * mtx)
{
	assert(mtx->locker == get_curr_thread());
	assert((mtx->thread_queue_front && mtx->thread_queue_back)
		|| (!mtx->thread_queue_front && !mtx->thread_queue_back));

	mtx->locker = NULL;
	if (mtx->thread_queue_front)
	{
		mtx->thread_queue_front->thread->state = RUNNABLE;
		thread_list_t * old_front = mtx->thread_queue_front;
		mtx->thread_queue_front = mtx->thread_queue_front->next;
		free(old_front);
	}

	return 0;
}
